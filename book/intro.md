# Welcome to the Linear Algebra Companion

This book is interactive: it has explanatory text sections together with code and exercises. Individual sections are further explained in Videos available on youtube.

The book is written with Jupyter Book. Check out [the Jupyter Book documentation](https://jupyterbook.org) for more information.


```{tableofcontents}
```
